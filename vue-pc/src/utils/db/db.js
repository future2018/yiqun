import cache from "@/utils/cache.js";
import store from "@/store/index.js";
var chatfix = "list_chat_";
var Msgfix = "list_msg_";
export const getChats = () => {
  return new Promise((resolve, reject) => {
    let chatList = cache.get(chatfix);
    if (chatList) {
      resolve(chatList);
    } else {
      reject(null);
    }
  });
};

export const getMsgList = chatObj => {
  return new Promise((resolve, reject) => {
    if (chatObj.chatType == 1) {
      let msgList = cache.get(Msgfix + chatObj.chatId);
      resolve(msgList);
    } else {
      let msgList = cache.get(Msgfix + chatObj.chatId);
      let msgListMyself = cache.get(Msgfix + store.state.user.operId);
      if (msgListMyself) {
        if (msgList) {
          msgListMyself.push(...msgList);
        }
        for (var i in msgListMyself) {
          if (msgListMyself[i].fromUserId == store.state.user.operId) {
            msgListMyself[i].isItMe = true;
          }
        }
        resolve(msgListMyself);
      }
    }
  });
};

export const openChat = chat => {
  return new Promise((resolve, reject) => {
    let chatList = cache.get(chatfix);
    if (chatList) {
      for (var i in chatList) {
        if (chatList[i].toUserId == chat.chatId) {
          chatList[i].unreadNumber = 0;
        }
      }
      cache.set(chatfix, chatList);
      store.commit("setConversation", chatList);
    }
  });
};

function addMsg(chat, chatId) {
  let MsgList = cache.get(Msgfix + chatId);
  if (!MsgList) {
    let MsgList2 = [];
    MsgList2.push(chat);
    cache.set(Msgfix + chatId, MsgList2);
    return;
  }
  if (MsgList.length >= 10) {
    MsgList.splice(0, 1);
  }
  MsgList.push(chat);
  cache.set(Msgfix + chatId, MsgList);
}

export const modifyPacket = chat => {
  let msglist = cache.get(Msgfix + chat.toUserId);
  if (!msglist) {
    let item = [];
    item.push(chat);
    cache.set(Msgfix + chat.toUserId, item);
    return;
  }
  for (var i in msglist) {
    if (msglist[i].hasBeenSentId == chat.hasBeenSentId) {
      msglist[i].content = chat.content;
    }
  }
  cache.set(Msgfix + chat.toUserId, msglist);
};

// 消息会话 消息增加
export const addChat = chat => {
  return new Promise((resolve, reject) => {
    if (chat.command == 3 || chat.command == 4) {
      if (chat.content == "") {
        return;
      }
      chat.unreadNumber = 1;
      chat.lastOpenTime = Date.now();

      if (chat.chatType == 1) {
        chat.chatId = chat.toUserId;
        chat.chatName = chat.toUserName;
        chat.avatar = chat.toUserHeadImg;
        if (chat.fromUserId == store.state.user.operId) {
          chat.isItMe = true;
        }
      } else {
        // 只拿自己的会话
        if (chat.toUserId == store.state.user.operId) {
          chat.chatName = chat.fromUserName;
          chat.chatId = chat.fromUserId;
          chat.avatar = chat.fromUserHeadImg;
        }
      }

      // 最新一条消息
      let chatList = cache.get(chatfix);
      if (!chatList) {
        let chatList2 = [];
        chatList2.push(chat);
        cache.set(chatfix, chatList2);
        store.commit("setConversation", chatList2);
        return;
      }

      var flag = false;
      for (var i in chatList) {
        if (chatList[i].chatId == chat.chatId) {
          chat.unreadNumber = chatList[i].unreadNumber + 1;
          chatList[i] = chat;
          flag = true;
        }
      }
      if (!flag) {
        chatList.push(chat);
        flag = false;
      }
      cache.set(chatfix, chatList);
      store.commit("setConversation", chatList);

      // 所有消息
      if (chat.contentType == 5) {
        modifyPacket(chat);
      } else {
        addMsg(chat, chat.toUserId);
      }
    }
  });
};
