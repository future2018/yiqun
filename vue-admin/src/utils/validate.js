/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * @param {string} url
 * @returns {Boolean}
 */
export function validURL(url) {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/**
 * @param {string} email
 * @returns {Boolean}
 */
export function validEmail(email) {
  const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return reg.test(email)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function isString(str) {
  if (typeof str === 'string' || str instanceof String) {
    return true
  }
  return false
}

/**
 * @param {Array} arg
 * @returns {Boolean}
 */
export function isArray(arg) {
  if (typeof Array.isArray === 'undefined') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return Array.isArray(arg)
}

/**
 * 判断空字符串
 * @param str
 * @returns {boolean}
 */
export function stringIsEmpty(content) {
  if (typeof content === 'undefined' || content === null || content === '') {
    return true
  }
  return false
}

// 校验身份证
export function checkIdCard(rule, value, callback) {
  const reg = /(^\d{8}(0\d|10|11|12)([0-2]\d|30|31)\d{3}$)|(^\d{6}(18|19|20)\d{2}(0\d|10|11|12)([0-2]\d|30|31)\d{3}(\d|X|x)$)/
  if (!value) {
    return callback(new Error('证件号码不能为空'))
  } else if (!reg.test(value)) {
    return callback(new Error('证件号码不正确'))
  } else {
    callback()
  }
}

// 验证 统一社会信用代码
export function verifyUnifyCreditIdentifyCode(rule, value, callback) {
  const reg = /[^_IOZSVa-z\W]{2}\d{6}[^_IOZSVa-z\W]{10}$/g
  if (!value) {
    return callback(new Error('统一信用代码不能为空'))
  } else if (value.length != 18) {
    return callback(new Error('统一信用代码位数不对'))
  } else if (!reg.test(value)) {
    return callback(new Error('统一信用代码不正确'))
  } else {
    callback()
  }
}

export function isMobile(rule, value, callback) {
  const reg = /^1[3|4|5|6|7|8|9][0-9]\d{8}$/
  if (!value) {
    return callback(new Error('手机号码不能为空'))
  } else if (value.length != 11) {
    return callback(new Error('手机号码位数不对'))
  } else if (!reg.test(value)) {
    return callback(new Error('手机号码不正确'))
  } else {
    callback()
  }
}

/**
 * @description 判断数据是否为座机号(固定电话)
 * @param {String} str_data 待校验的数据
 * @returns {Boolean}, true:是座机号
 **/
export function isTelephone(rule, value, callback) {
  value = value || String(this);
  if (value.match(/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/) == null) {
    return callback(new Error('座机号不正确'))
  } else {
    return callback()
  }
}

/**
 * @description 判断是否为银行卡号
 * @param {String} value 待校验的数据
 * @returns {Boolean}, true:是银行卡号
 **/
export function isBankCard(rule, value, callback) {
  value = value || String(this);
  if ("" == value.trim() || undefined == value) {
    return callback(new Error('银行卡号不能为空'))
  }
  let lastNum = value.substr(value.length - 1, 1);//取出最后一位（与luhm进行比较）

  let first15Num = value.substr(0, value.length - 1);//前15或18位
  let newArr=new Array();
  for(let i=first15Num.length-1;i>-1;i--){    //前15或18位倒序存进数组
    newArr.push(first15Num.substr(i,1));
  }
  let arrJiShu=new Array();  //奇数位*2的积 <9
  let arrJiShu2=new Array(); //奇数位*2的积 >9

  let arrOuShu=new Array();  //偶数位数组
  for(let j=0;j<newArr.length;j++){
    if((j+1)%2==1){//奇数位
      if(parseInt(newArr[j])*2<9)
        arrJiShu.push(parseInt(newArr[j])*2);
      else
        arrJiShu2.push(parseInt(newArr[j])*2);
    }
    else //偶数位
      arrOuShu.push(newArr[j]);
  }

  let jishu_child1=new Array();//奇数位*2 >9 的分割之后的数组个位数
  let jishu_child2=new Array();//奇数位*2 >9 的分割之后的数组十位数
  for(let h=0;h<arrJiShu2.length;h++){
    jishu_child1.push(parseInt(arrJiShu2[h])%10);
    jishu_child2.push(parseInt(arrJiShu2[h])/10);
  }

  let sumJiShu=0; //奇数位*2 < 9 的数组之和
  let sumOuShu=0; //偶数位数组之和
  let sumJiShuChild1=0; //奇数位*2 >9 的分割之后的数组个位数之和
  let sumJiShuChild2=0; //奇数位*2 >9 的分割之后的数组十位数之和
  let sumTotal=0;
  for(let m=0;m<arrJiShu.length;m++){
    sumJiShu=sumJiShu+parseInt(arrJiShu[m]);
  }

  for(let n=0;n<arrOuShu.length;n++){
    sumOuShu=sumOuShu+parseInt(arrOuShu[n]);
  }

  for(let p=0;p<jishu_child1.length;p++){
    sumJiShuChild1=sumJiShuChild1+parseInt(jishu_child1[p]);
    sumJiShuChild2=sumJiShuChild2+parseInt(jishu_child2[p]);
  }
  //计算总和
  sumTotal=parseInt(sumJiShu)+parseInt(sumOuShu)+parseInt(sumJiShuChild1)+parseInt(sumJiShuChild2);

  //计算Luhm值
  let k= parseInt(sumTotal)%10==0?10:parseInt(sumTotal)%10;
  let luhm= 10-k;

  if(lastNum==luhm){
    return callback()
  } else{
    return callback(new Error('银行卡号不正确'))
  }
}

