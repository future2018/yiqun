
[![输开源协议](https://img.shields.io/badge/License-Apache--2.0-brightgreen.svg "Apache")](https://www.apache.org/licenses/LICENSE-2.0)

[![maven最新版本](https://maven-badges.herokuapp.com/maven-central/org.j-im/jim-core/badge.svg "maven最新版本")](https://maven-badges.herokuapp.com/maven-central/org.j-im/jim-core)


## ![](/assets/logo.png)bzGhost简介
支持所有端的聊天软件，高仿微信，用到的技术栈 electron uniapp netty java 支持文字红包 聊天 ，支持群组建群等功能。有管理端 安卓端 苹果端 window端 mac端 linux端等。
bzGhost 打造跨终端跨平台即时通讯，个人也能玩的转的聊天软件。
## 官方网站

[http://yuyaogc.com/](http://yuyaogc.com/)


![](/assets/banner.jpg)


## 版权信息

软件遵循[MIT](https://baike.baidu.com/item/MIT/10772952)开源协议，意味着您无需支付任何费用，也无需授权，即可将 软件应用到您的产品中。  
注意：这并不意味着您可以将软件应用到非法的领域，比如涉及赌博、色情、暴力、宗教等方面。
如因此产生纠纷等法律问题， 作者不承担任何责任。切勿以身试法!!! 网络不是法外之地


## 产品展示
![](/assets/admin_group_02.png)
![](/assets/chat.jpg)
![](/assets/redpacket.jpg)
![](/assets/redpacketdetail.jpg)
![](/assets/pc_chat_03.png)
![](/assets/pc_friend_04.png)

## 演示地址

| 链接 | 语言 | 版本 | 开源 |
| :--- | :--- | :--- | :--- |
| [后台管理](http://42.193.146.14:8089/index.html) | Vue | 1.0.0 | 100% |
| [安卓端](https://gitee.com/Huiyun-Co/yiqun/raw/master/assets/__UNI__0E0525C__20220321202044.apk) | Uniapp | 1.0.0 | 100% |
| [电脑端](https://gitee.com/Huiyun-Co/yiqun/raw/master/assets/bzGhost3.0.0.exe) | Electron | 1.0.0 | 100% |
| H5端 | Uniapp | 1.0.0 | 100% |
| Web端 | Electron | 1.0.0 | 100% |
| 通信 | Netty | 1.0.0 | 100% |
| 接口 | Java | 1.0.0 | 有偿 |

### 1.5 账号说明

\*\*注意：
管理员账号：admin /123456

测试账号：test/123456
## 参考文献

* [https://gitee.com/lele-666/V-IM.git](https://gitee.com/lele-666/V-IM.git)
* [http://doc.ruoyi.vip/ruoyi/](http://doc.ruoyi.vip/ruoyi/)
* [https://gitee.com/xiaowang0482/wechat.git](https://gitee.com/xiaowang0482/wechat.git)


## 联系方式

* 如有问题联系作者 微信 18767176707
* 讨论加群：qq群 [^1]1025293030
